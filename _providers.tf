provider "aws" {
  region = "ap-southeast-2"
}

provider "aws" {
  alias  = "idp"
  region = "ap-southeast-2"

  assume_role {
    role_arn = "arn:aws:iam::${module.account_idp.account_id}:role/${var.org_role}"
  }
}

provider "aws" {
  alias  = "mgmt"
  region = "ap-southeast-2"

  assume_role {
    role_arn = "arn:aws:iam::${module.account_mgmt.account_id}:role/${var.org_role}"
  }
}

provider "aws" {
  alias  = "nonprod"
  region = "ap-southeast-2"

  assume_role {
    role_arn = "arn:aws:iam::${module.account_nonprod.account_id}:role/${var.org_role}"
  }
}

provider "aws" {
  alias  = "prod"
  region = "ap-southeast-2"

  assume_role {
    role_arn = "arn:aws:iam::${module.account_prod.account_id}:role/${var.org_role}"
  }
}