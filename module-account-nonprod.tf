module "account_nonprod" {
  source = "git::https://github.com/DNXLabs/terraform-aws-account.git?ref=0.0.4"

  name   = "${var.org_name}-nonprod"
  # Always use a valid email
  email  = "aws+${var.org_name}-nonprod@${var.email_suffix}"
}

output "nonprod_account_id" {
  value = "${module.account_nonprod.account_id}"
}
