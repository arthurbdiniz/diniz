module "idp_gsuite" {
  providers = {
    aws = "aws.idp"
  }

  source = "git::https://github.com/DNXLabs/terraform-aws-idp-gsuite.git?ref=1.0.0"

  org_name                    = "${var.org_name}"
  metadata                    = "${file("gsuite-metadata.xml")}"
  client_all_admin_role_names = []
  extra_roles                 = ["developer"]
}

module "idp_security" {
  providers = {
    "aws" = "aws.idp"
  }

  source = "git::https://github.com/DNXLabs/terraform-aws-account-security.git?ref=1.0.0"

  org_name     = "${var.org_name}"
  account_name = "idp"
}