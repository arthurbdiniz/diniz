data "aws_iam_policy_document" "nonprod_developer" {
  statement {
    actions   = ["*"]
    resources = ["*"]
  }

  statement {
    effect  = "Deny"
    actions = ["iam:*"]

    resources = ["arn:aws:iam::*:role/${var.org_name}-nonprod-*"]
  }
}

module "nonprod_security" {
  providers = {
    "aws" = "aws.nonprod"
  }

  source = "git::https://github.com/DNXLabs/terraform-aws-account-security.git?ref=1.0.0"

  org_name               = "${var.org_name}"
  account_name           = "nonprod"
  idp_account_id         = "${module.account_idp.account_id}"
  iam_ci_mgmt_account_id = "${module.account_mgmt.account_id}"

  extra_roles = {
    "developer" = "${data.aws_iam_policy_document.nonprod_developer.json}"
  }

  idp_admin_trust_account_ids = ["${var.dnx_idp_account_id}"]
}
