# GSuite SAML Setup

## Setting up roles for users

To be able to login to AWS using your Google account, you need to add the IAM Roles:

- Find your user
- Click _User Information_
- Scroll to _AWS SAML_
- Add a new IAM Role to the list:

```
arn:aws:iam::<AWS_ACCOUNT_ID>:role/dnx-admin,arn:aws:iam::<AWS_ACCOUNT_ID>:saml-provider/GSuite
```

Where `AWS_ACCOUNT_ID` is the IDP account ID.

> Remember that the Google user has to the in the same Google account as the SAML metadata obtained before.

## Settimg up Makefile GSuite IDP Variables

In the `Makefile`, if the Google Account used is not DNX's, update the following variables:

```
export GOOGLE_IDP_ID=
export GOOGLE_SP_ID=
```

To find those values, go to Google Admin, under Apps > SAML Apps > Settings for AWS SSO -- the URL will include a component that looks like ...#AppDetails:service=123456789012... -- that number is `GOOGLE_SP_ID`

Again in Google Admin, under Security > Set up single sign-on (SSO) -- the SSO URL includes a string like https://accounts.google.com/o/saml2/idp?idpid=aBcD01AbC where the last bit (after the =) is `GOOGLE_IDP_ID`.
