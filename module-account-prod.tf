module "account_prod" {
  source = "git::https://github.com/DNXLabs/terraform-aws-account.git?ref=0.0.4"
  
  name   = "${var.org_name}-prod"
  # Always use a valid email
  email  = "aws+${var.org_name}-prod@${var.email_suffix}"
}

output "prod_account_id" {
  value = "${module.account_prod.account_id}"
}
