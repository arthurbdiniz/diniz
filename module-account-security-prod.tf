module "prod_security" {
  providers = {
    "aws" = "aws.prod"
  }

  source = "git::https://github.com/DNXLabs/terraform-aws-account-security.git?ref=1.0.0"

  org_name               = "${var.org_name}"
  account_name           = "prod"
  idp_account_id         = "${module.account_idp.account_id}"
  iam_ci_mgmt_account_id = "${module.account_mgmt.account_id}"

  idp_admin_trust_account_ids = ["${var.dnx_idp_account_id}"]
}
