module "mgmt_security" {
  providers = {
    "aws" = "aws.mgmt"
  }

  source = "git::https://github.com/DNXLabs/terraform-aws-account-security.git?ref=1.0.0"

  org_name               = "${var.org_name}"
  account_name           = "mgmt"
  idp_account_id         = "${module.account_idp.account_id}"
  iam_ci_mgmt            = true
  iam_ci_mgmt_account_id = "${module.account_mgmt.account_id}"

  ssm_account_names = ["${var.org_name}-mgmt", "${var.org_name}-nonprod", "${var.org_name}-prod"]

  ssm_account_ids = [
    "${module.account_mgmt.account_id}",
    "${module.account_nonprod.account_id}",
    "${module.account_prod.account_id}"
  ]

  idp_admin_trust_account_ids = ["${var.dnx_idp_account_id}"]
}
