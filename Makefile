export GOOGLE_IDP_ID=C01501d06#DNX IDP
export GOOGLE_SP_ID=192607830114#DNX ID

export AWS_DEFAULT_REGION=ap-southeast-2

env-%:
	@ if [ "${${*}}" = "" ]; then \
			echo "Environment variable $* not set"; \
			exit 1; \
	fi

.env:
	@echo "make .env"
	cp .env.template .env
	echo >> .env
	touch .env.auth
	touch .env.assume

.env.assume: .env env-AWS_ACCOUNT_ID
	@echo "make .env.assume"
	echo > .env.assume
	docker-compose -f docker-compose.yml pull aws
	docker-compose -f docker-compose.yml run -e AWS_ROLE=dnx-root --rm aws assume-role.sh > .env.assume

init: .env
	docker-compose run --rm terraform init
.PHONY: init

shell: .env
	docker-compose run --rm \
		--entrypoint "" \
		terraform bash
.PHONY: shell

apply: .env
	echo "+++ :terraform: Terraform Apply"
	docker-compose run --rm \
		terraform apply .terraform-plan
.PHONY: apply
 
plan: .env
	echo "+++ :terraform: Terraform Plan"
	docker-compose run --rm \
		terraform plan -out=.terraform-plan
.PHONY: plan

check_clean:
	@echo "Are you sure you want to delete the files? It's including (terraform files, gsuite xml and git repository) [yes/no] " && read ans && [ $${ans:-N} == yes ]
.PHONY: clean check_clean

clean: check_clean .env
	echo "+++ :system: Cleaning default files"
	docker-compose run --rm --entrypoint="rm -rf .terraform terraform.tfstate* terraform.tfstate.d gsuite-metadata.xml .git ||:" terraform