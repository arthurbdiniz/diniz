module "tf_backend" {
  source = "git::https://github.com/DNXLabs/terraform-aws-backend?ref=1.0.0"
  
  providers = {
    "aws" = "aws.mgmt"
  }

  bucket_prefix = "${var.org_name}"
  bucket_region = "${data.aws_region.current.name}"

  bucket_sse_algorithm = "AES256"

  workspaces = "${var.tf_workspaces}"

  assume_policy = {
    # add to all: 
    # - IDP_ACCOUNT_ID:root     # for roles running from CLI
    # - MGMT_ACCOUNT_ID:root    # for when running from CI
    # - DNX_IDP_ACCOUNT_ID:root # to allow DNX
    all = "arn:aws:iam::${module.account_idp.account_id}:root,arn:aws:iam::${module.account_mgmt.account_id}:root,arn:aws:iam::${var.dnx_idp_account_id}:root"

    prod    = "arn:aws:iam::${module.account_idp.account_id}:root"
    nonprod = "arn:aws:iam::${module.account_idp.account_id}:root"
    mgmt    = "arn:aws:iam::${module.account_idp.account_id}:root"
    idp     = "arn:aws:iam::${module.account_idp.account_id}:root"
  }
}