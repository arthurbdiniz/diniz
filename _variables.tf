variable "org_name" {
  description = "Name of this organization (slug)"
  default     = "bubbletea"
}

variable "org_role" {
  description = "Role for organization"
  default     = "OrganizationAccountAccessRole"
}

variable "email_suffix" {
  default = "dnx.solutions"
}

variable "tf_workspaces" {
  description = "List of account names that reflect workspaces prefixes in terraform"
  default = ["prod", "nonprod", "mgmt", "idp"]
}

variable "dnx_idp_account_id" {
  description = "DNX IDP Account ID to trust"
  default = "148642451802"
}