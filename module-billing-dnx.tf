module "billing_dnx" {
  source = "git::https://github.com/DNXLabs/terraform-aws-billing-role.git?ref=1.0.0"

  org_name       = "dnx"
  idp_account_id = "148642451802"
}