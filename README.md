# DNX - Well Architected AWS Foundations

This Document presents all details on <ClientName> AWS Structure.
All configurations were applied using Infrastructure as Code by terraform using DNX One modules.
The image below summarises the whole AWS Architecture

![AWS HLD](images/hld_aws.png)

## Table of Contents

1. [Account Structure](#AccountStructure)
2. [Networking](#Networking)
3. [DNS](#DNS)
4. [Security](#Security)
5. [ECR](#ECR)
6. [ECS](#ECS)
7. [Database](#Database)
8. [CICD](#CICD)
9. [Deployment](#Deployment)

## AccountStructure

| AWS Account    | Number       | Description                                                                                                                                                         |
| -------------- | ------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Master         | 804075007277 | Root Organization account containing: Consolidate Billing, AWS Organization, CloudTrail S3 Buckets                                                                  |
| IDP            | 148642451802 | IDP Account is responsible to land users from third party IDP providers (Azure Ad) using SAML2                                                                      |
| Mgmt           | 335071203852 | Mgmt account has any shared services to be consumed by the AWS structure: CICD Agents, ECR repositories                                                             |
| Non-Production | 663974054766 | All application workloads for non-production should run into this account. AWS Networking, ECS Clusters, ALBs and Route53 Hosted Zones are hosted into this account |
| Production     | 274939991404 | All application workloads for production should run into this account. AWS Networking, ECS Clusters, ALBs and Route53 Hosted Zones are hosted into this account     |

## Networking

All configurations were applied using Infrastructure as Code by terraform and the related git project can be found at: infra-<ClientName>-network

NonProd, Production and Mgmt accounts are allowed to have workloads, therefore, the following VPCs have been created:

| AWS Account | Cidr         | Description       |
| ----------- | ------------ | ----------------- |
| Mgmt        | 10.37.0.0/16 | Number of Nats: 1 |
| Nonprod     | 10.47.0.0/16 | Number of Nats: 1 |
| Production  | 10.57.0.0/16 | Number of Nats: 3 |

### Subnets

Inside every VPC a number of subnets were created to manage networking traffic within the VPC and apply a robust security layer using concepts as defence in depth:

| Subnets Layer | Description                                                                                                                                                                                                                 |
| ------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Public DMZ    | This layer has public access, typically all workloads here will have a public access endpoint. AWS LoadBalancers and AWS NATs will be deployed into this layer                                                              |
| Private       | Private subnets are not public access, only public and secure subnets can reach this layer. All outbound requests are routed to NATs deployed on the public layer. Application workloads should be deployed into this layer |
| Secure        | Secure subnets are not public access, they are only accessible from Private subnets and are ideal for data storage services                                                                                                 |

Intra subnet communications are handled by router rules and NACLs (Networking Access Control list). This is the foundations for security in depth and strong policies to deny unauthorized traffic into subnets.

### NACLS

![AWS Security HLD](images/hld_aws_networking.png)

## DNS

The AWS Foundation stack has multiple AWS Accounts and each one has a Route53 Hosted zone, each environment (mgmt, nonprod and production) will have a valid DNS entry and a valid SSL certificated managed by AWS ACM.

All configurations were applied using Infrastructure as Code by terraform and the related git project can be found at: infra-waterco-domain

| Account | Subdomain                                    |
| ------- | -------------------------------------------- |
| Prod    | cloud.dnx.hosting and prod.cloud.dnx.hosting |
| NonProd | nonprod.cloud.dnx.hosting                    |
| Mgmt    | mgmt.cloud.dnx.hosting                       |

The whole AWS domain is hosted in the prod account. Therefore only for prod, the Route53 has 2 hosted zones:

- cloud.dnx.hosting
- prod.cloud.dnx.hosting

The cloud.dnx.hosting hosted zone has the entries for mgmt and nonprod domains (NS records)

All application-related names should be created using the appropriate account hosted zone:

- mgmt.cloud.dnx.hosting
- nonprod.cloud.dnx.hosting
- prod.cloud.dnx.hosting

![AWS DNS HLD](images/hld_aws_dns.png)

## Security

The AWS Foundation stack applies a set of IAM resources across the AWS accounts to apply the concept of least privilege and provide exactly the needed policy at the time for users and systems.

All configurations were applied using Infrastructure as Code by terraform and the related git project can be found at: infra-<ClientName>-root-security

In this platform, most of the regular access to AWS cloud will be using SSO (Single Sign On) using <ClientName> Azure AD. This SSO configuration will generate sts tokens to allow temporary access into IDP Account only. Once logged into IDP users should assume roles to specified accounts (mgmt,nonprod and prod)

The exception flow occurs for Gitlab CICD Server, this instance runs outside AWS cloud and requires static credentials to deploy workloads across environments.

DNX One comes with 3 different roles applied into the environments:

| IAM Role                             | Description                                     |
| ------------------------------------ | ----------------------------------------------- |
| <ClientName>-<AccountName>-admin     | Full Admin role, has access to all AWS Services |
| <ClientName>-<AccountName>-readonly  | ReadOnly role for all AWS Services              |
| <ClientName>-<AccountName>-developer | Restricted role for day to day developer tasks  |

![AWS IAM HLD](images/hld_aws_security.png)

## Application Layer

This section describes the general architecture used by <ClientName> to deploy applications on the AWS Platform following the below rules:

- All applications are packed using Docker containers
- Applications are stateless
- AWS ECR is used to store Docker images
- AWS ECS is used to orchestrate containers
- AWS ALB is used in front of every application
- AWS Route53 has an A record for every application within each environment
- In the case of stateful apps, an AWS EFS volume is provisioned to the cluster

## ECR

All docker applications use ECR to store Docker images. The code to create new ECR repositories is at : infra-ClientName-mgmt-ci
The following code shows an <ClientName> repository using terraform

```bash
module "ecr_app-name" {
  source = "git::https://github.com/DNXLabs/terraform-aws-ecr.git?ref=0.0.1"
  name   = "app-name-api"

  trust_accounts = [
    "${data.aws_ssm_parameter.mgmt_account_id.value}",
    "${data.aws_ssm_parameter.nonprod_account_id.value}",
    "${data.aws_ssm_parameter.prod_account_id.value}",
  ]
}
```

The ECR repository is located in the Mgmt AWS Account and the High-level integration with nonprod and production accounts is shown below:

![AWS ECR HLD](images/hld_aws_ecr.png)

## ECS

For all docker applications at <ClientName> the AWS ECS (Elastic Container Service) will be used to orchestrate the application and ensure the correct desire state.

DNX One provides a comprehensive module to deploy a well architect application using ECS, ECR, ALB, EFS, CodeDeploy and Rout53 using Blue/Green deployment and zero downtime on each deployment.

All necessary terraform code can be found at infra-ClientName-ecs-platform.

The follow terraform code demontrates a default stack on ecs that is shares with all <ClientName> Applications.

```bash
module "ecs_apps" {
  source               = "git::https://github.com/DNXLabs/terraform-aws-ecs.git?ref=0.0.2"
  name                 = "${local.workspace["cluster_name"]}"
  instance_type_1      = "t2.medium"
  instance_type_2      = "t2.large"
  instance_type_3      = "t2.nano"
  vpc_id               = "${data.aws_vpc.selected.id}"
  private_subnet_ids   = ["${data.aws_subnet_ids.private.ids}"]
  public_subnet_ids    = ["${data.aws_subnet_ids.public.ids}"]
  secure_subnet_ids    = ["${data.aws_subnet_ids.secure.ids}"]
  certificate_arn      = "${data.aws_acm_certificate.domain_host.arn}"
  on_demand_percentage = 0
}
```

The code below demonstrates the terraform code for each application

```bash
module "ecs_app_core_suppliers_api_01" {
  source                 = "git::https://github.com/DNXLabs/terraform-aws-ecs-app.git?ref=0.0.1"
  vpc_id                 = "${data.aws_vpc.selected.id}"
  cluster_name           = "${module.ecs_apps.ecs_name}"
  service_role_arn       = "${module.ecs_apps.ecs_service_iam_role_arn}"
  task_role_arn          = "${module.ecs_apps.ecs_task_iam_role_arn}"
  alb_listener_https_arn = "${element(module.ecs_apps.alb_listener_https_arn, 0)}"
  alb_dns_name           = "${element(module.ecs_apps.alb_dns_name, 0)}"
  name                   = "api"
  image                  = "dnxsolutions/nginx-hello:latest"
  container_port         = 80
  hostname               = "api.${local.workspace["hosted_zone"]}"
  hostname_blue          = "api-blue.${local.workspace["hosted_zone"]}"
  hostname_origin        = "api-origin.${local.workspace["hosted_zone"]}"
  hosted_zone            = "${local.workspace["hosted_zone"]}"
  healthcheck_path       = "/api/healthcheck"
  certificate_arn        = "${data.aws_acm_certificate.domain_host_us.arn}"
}
```

### Some important points on this stack

- There is one ECS cluster per environment
- All ec2 machines are provisioned using AWS Spot instances (saving up to 70% on costs)
- The ec2 machines are provisioned into the Private Subnet
- One Application Load Balancer is provisioned on the Public DMZ Layer
- One Route53 A record is created for each application
- One ACM SSL Certificate is attached to the Route53 hosted zone
- One EFS volume is attached to this state for any stateful application

![AWS ECS HLD](images/hld_aws_ecs.png)

## Database

<ClientName> uses PostgreSQL as default database choice. On AWS the database will be provisioned using AWS RDS (Relational Database Service), which is a managed services that is responsible to keep the database updated, high available and daily snapshots are saved to ensure DR features.

The terraform code to supply the PostgreSQL across the environment can be found at infra-<ClientName>-ecs-platform (rds-db.tf)

```bash
resource "aws_db_instance" "client-db" {
  identifier                = "${local.workspace["db_name"]}"
  engine                    = "sqlserver-ex"
  engine_version            = "14.00.1000.169.v1"
  instance_class            = "db.t2.micro"
  storage_encrypted         = false
  license_model             = "license-included"
  port                      = "1433"
  multi_az                  = "${local.workspace["db_mult_az"]}"
  allocated_storage         = "20"
  final_snapshot_identifier = "${local.workspace["db_name"]}-snapshot"
```

AWS RDS has an optional Mult-AZ deployment where it provides high availability feature with one master database and on a secondary database, each one in a different availability zone.

![AWS ECS HLD](images/hld_aws_rds.png)

## CICD

All applications have a Gitlab Continuous Integration and Continuous Delivery pipeline (CI/CD) the pipelines rules can be checked on the .gitlab-ci.yml file

The pipeline below demonstrates all pipeline steps

```yaml
image: dnxsolutions/musketeers:1.1.1-ecr
services:
  - docker:18.03.1-ce-dind # needs to match gitlab runner version
variables:
  NODE_PATH: node_modules/:src/
  DOCKER_HOST: tcp://docker:2375/
  DOCKER_DRIVER: overlay2
  BUILD_VERSION: $CI_COMMIT_SHORT_SHA
  AWS_ACCOUNT_ID: #Mgmt Account
  AWS_ROLE: ci-deploy
  AWS_DEFAULT_REGION: ap-southeast-2
  CONTAINER_PORT: 3000

cache:
  key: ${CI_COMMIT_REF_SLUG}
  paths:
    - node_modules/
    - package.json
    - package-lock.json

stages:
  - install
  - test
  - dockerBuildPush
  - deploy

libraries:
  tags: [ClientName_runner, docker]
  stage: install
  script:
    - make install
  cache:
    policy: push
  retry: 2
  artifacts:
    paths:
      - node_modules/
      - package.json
      - package-lock.json
    expire_in: 30 minutes

unit-tests:
  tags: [ClientName_runner, docker]
  stage: test
  script:
    - make test
  dependencies:
    - libraries
  cache:
    policy: pull-push
  retry: 2
dockerBuildAndPush:
  when: on_success
  tags: [ClientName_runner, docker]
  stage: dockerBuildPush
  script:
    - make dockerBuild dockerPush
  only:
    - master
  cache:
    policy: push

"Deploy NonProd":
  tags: [ClientName_runner, docker]
  when: on_success
  stage: deploy
  variables:
    AWS_ACCOUNT_ID: #NONPROD
    AWS_ENV: nonprod
  script:
    - make deploy
  only:
    - master
  cache:
    policy: push
```

![AWS ECS HLD](images/hld_aws_cicd.png)

## Deployment

Every deployment using this architecture happens with a zero-downtime deployment.
The image below depicts the steps around the blue/green deployment

![AWS Deployment HLD](images/hld_aws_deployment.png)
