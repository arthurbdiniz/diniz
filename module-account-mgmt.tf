module "account_mgmt" {
  source = "git::https://github.com/DNXLabs/terraform-aws-account.git?ref=0.0.4"

  name   = "${var.org_name}-mgmt"
  # Always use a valid email
  email  = "aws+${var.org_name}-mgmt@${var.email_suffix}"
}

output "mgmt_account_id" {
  value = "${module.account_mgmt.account_id}"
}
