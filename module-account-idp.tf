module "account_idp" {
  source = "git::https://github.com/DNXLabs/terraform-aws-account.git?ref=0.0.4"
  
  name   = "${var.org_name}-idp"
  # Always use a valid email
  email  = "aws+${var.org_name}-idp@${var.email_suffix}"
}

output "idp_account_id" {
  value = "${module.account_idp.account_id}"
}
