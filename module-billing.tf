module "billing" {
  source = "git::https://github.com/DNXLabs/terraform-aws-billing-role.git?ref=1.0.0"

  org_name   = "${var.org_name}"
  idp_account_id = "${module.account_idp.account_id}"
}
